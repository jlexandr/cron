package deliveroo.model;

import java.util.Map;

public class DayOfWeek extends CronPart {

    public DayOfWeek(String s) {
        super(s);
    }

    @Override
    protected int getRangeMin() {
        return 0;
    }

    @Override
    protected int getRangeMax() {
        return 6;
    }

    @Override
    protected Map<String, Integer> getSpecialValues() {
        return Map.ofEntries(
                Map.entry("SUN", 0),
                Map.entry("MON", 1),
                Map.entry("TUE", 2),
                Map.entry("WED", 3),
                Map.entry("THU", 4),
                Map.entry("FRI", 5),
                Map.entry("SAT", 6)
        );
    }
}
