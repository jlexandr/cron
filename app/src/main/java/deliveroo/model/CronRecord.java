package deliveroo.model;

import java.text.ParseException;

public class CronRecord {

    private static final int AMOUNT_OF_ARGUMENTS = 6;

    private Minute minute;
    private Hour hour;
    private DayOfMonth dayOfMonth;
    private Month month;
    private DayOfWeek dayOfWeek;
    private String command;

    public CronRecord() {
    }

    private CronRecord(Builder builder) {
        setMinute(builder.minute);
        setHour(builder.hour);
        setDayOfMonth(builder.dayOfMonth);
        setMonth(builder.month);
        setDayOfWeek(builder.dayOfWeek);
        setCommand(builder.command);
    }

    public static CronRecord parseFromString(String s) throws ParseException {
        try {
            String[] a = s.split(" ");
            if (a.length != AMOUNT_OF_ARGUMENTS){
                throw new ParseException(String.format("Can not parse CronRecord from %s : " +
                        "Unexpected arguments amount. Expected: %s, actual: %s",
                        s, AMOUNT_OF_ARGUMENTS, a.length), 0);
            }
            return newBuilder()
                    .minute(new Minute(a[0]))
                    .hour(new Hour(a[1]))
                    .dayOfMonth(new DayOfMonth(a[2]))
                    .month(new Month(a[3]))
                    .dayOfWeek(new DayOfWeek(a[4]))
                    .command(a[5])
                    .build();
        } catch (ParseException parseException){
            throw parseException;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ParseException(String.format("Can not parse CronRecord from %s", s), 0);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String humanReadableFormat() {
        StringBuilder sb = new StringBuilder();
        sb.append("minute \t").append(minute)
                .append("\nhour \t").append(hour)
                .append("\nday of month \t").append(dayOfMonth)
                .append("\nmonth \t").append(month)
                .append("\nday of week \t").append(dayOfWeek)
                .append("\ncommand \t").append(command).append("\n");
        return sb.toString();
    }

    public Minute getMinute() {
        return minute;
    }

    public void setMinute(Minute minute) {
        this.minute = minute;
    }

    public Hour getHour() {
        return hour;
    }

    public void setHour(Hour hour) {
        this.hour = hour;
    }

    public DayOfMonth getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(DayOfMonth dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }


    public static final class Builder {
        private Minute minute;
        private Hour hour;
        private DayOfMonth dayOfMonth;
        private Month month;
        private DayOfWeek dayOfWeek;
        private String command;

        private Builder() {
        }

        public Builder minute(Minute minute) {
            this.minute = minute;
            return this;
        }

        public Builder hour(Hour hour) {
            this.hour = hour;
            return this;
        }

        public Builder dayOfMonth(DayOfMonth dayOfMonth) {
            this.dayOfMonth = dayOfMonth;
            return this;
        }

        public Builder month(Month month) {
            this.month = month;
            return this;
        }

        public Builder dayOfWeek(DayOfWeek dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
            return this;
        }

        public Builder command(String command) {
            this.command = command;
            return this;
        }

        public CronRecord build() {
            return new CronRecord(this);
        }
    }
}
