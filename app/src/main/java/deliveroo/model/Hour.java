package deliveroo.model;

public class Hour extends CronPart {

    public Hour(String s) {
        super(s);
    }

    @Override
    protected int getRangeMin() {
        return 0;
    }

    @Override
    protected int getRangeMax() {
        return 23;
    }

}
