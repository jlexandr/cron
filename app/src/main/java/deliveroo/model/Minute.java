package deliveroo.model;

public class Minute extends CronPart {

    public Minute(String s) {
        super(s);
    }

    @Override
    protected int getRangeMin() {
        return 0;
    }

    @Override
    protected int getRangeMax() {
        return 59;
    }

}
