package deliveroo.model;

import java.util.Map;

public class Month extends CronPart {

    public Month(String s) {
        super(s);
    }

    @Override
    protected int getRangeMin() {
        return 1;
    }

    @Override
    protected int getRangeMax() {
        return 12;
    }

    @Override
    protected Map<String, Integer> getSpecialValues() {
        return Map.ofEntries(
                Map.entry("JAN", 1),
                Map.entry("FEB", 2),
                Map.entry("MAR", 3),
                Map.entry("APR", 4),
                Map.entry("MAY", 5),
                Map.entry("JUN", 6),
                Map.entry("JUL", 7),
                Map.entry("AUG", 8),
                Map.entry("SEPT", 9),
                Map.entry("OCT", 10),
                Map.entry("NOV", 11),
                Map.entry("DEC", 12)
        );
    }

}
