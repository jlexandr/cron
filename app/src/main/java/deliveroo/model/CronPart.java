package deliveroo.model;

import deliveroo.validation.Validator;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public abstract class CronPart {

    protected String encoded;

    public CronPart(String encoded) {
        try {
            this.encoded = encoded;
            validate();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
    }

    public void validate() throws ParseException {
        Validator validator = Validator.newBuilder()
                .minValue(getRangeMin())
                .maxValue(getRangeMax())
                .specialValues(getSpecialValues())
                .build();
        validator.validate(getEncoded());
    }

    public String toString() {
        List<String> result = new ArrayList<>();
        String encoded = replaceSpecialValuesWithInt(getEncoded(), getSpecialValues());
        if (encoded.equals("*")) {
            for (int i = getRangeMin(); i <= getRangeMax(); i++) {
                result.add(String.valueOf(i));
            }
        } else if (encoded.contains("/")) {
            int delimiter = Integer.parseInt(encoded.split("/")[1]);
            for (int i = getRangeMin(); i <= getRangeMax(); i++) {
                if (i % delimiter == 0) {
                    result.add(String.valueOf(i));
                }
            }
        } else if (encoded.contains("-")) {
            String[] arr = encoded.split("-");
            int start = Integer.parseInt(arr[0]);
            int end = Integer.parseInt(arr[1]);
            for (int i = start; i <= end; i++) {
                result.add(String.valueOf(i));
            }
        } else if (encoded.contains(",")) {
            Collections.addAll(result, encoded.split(","));
        } else {
            result.add(encoded);
        }
        return result.stream().collect(Collectors.joining(" "));
    }

    public static String replaceSpecialValuesWithInt(String encoded,
                                                     Map<String, Integer> specialValues) {
        for(Map.Entry<String, Integer> entry : specialValues.entrySet()){
            encoded = encoded.replace(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return encoded;
    }

    protected abstract int getRangeMin();

    protected abstract int getRangeMax();

    protected Map<String, Integer> getSpecialValues() {
        return new HashMap<>();
    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }
    
}
