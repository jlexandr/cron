package deliveroo.model;

public class DayOfMonth extends CronPart {

    public DayOfMonth(String s) {
        super(s);
    }

    @Override
    protected int getRangeMin() {
        return 1;
    }

    @Override
    protected int getRangeMax() {
        return 31;
    }

}
