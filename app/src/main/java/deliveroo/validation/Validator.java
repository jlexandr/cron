package deliveroo.validation;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import static deliveroo.model.CronPart.replaceSpecialValuesWithInt;

public class Validator {

    private int minValue;
    private int maxValue;
    private Map<String, Integer> specialValues;

    private Validator(Builder builder) {
        minValue = builder.minValue;
        maxValue = builder.maxValue;
        specialValues = builder.specialValues;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public void validate(String encoded) throws AssertionError  {
        assert !encoded.contains(" ");
        boolean containsSpecialChar = false;
        encoded = replaceSpecialValuesWithInt(encoded, getSpecialValues());
        if (encoded.contains("-")){
            containsSpecialChar = true;
            String[] arr = encoded.split("-");
            assert arr.length == 2;
            validateRange(arr);
            assert Integer.parseInt(arr[0]) < Integer.parseInt(arr[1]);
        }
        if (encoded.contains(",")){
            assert !containsSpecialChar;
            containsSpecialChar = true;
            validateRange(encoded.split(","));
        }
        if (encoded.contains("/")){
            assert !containsSpecialChar;
            containsSpecialChar = true;
            String[] arr = encoded.split("/");
            assert arr.length == 2;
            assert arr[0].equals("*");
            validateRange(Integer.parseInt(arr[1]));
        }
        if (!containsSpecialChar && !encoded.equals("*")){
            validateRange(Integer.parseInt(encoded));
        }
    }

    private void validateRange(String[] arr){
        Arrays.stream(arr).forEach(s -> validateRange(Integer.parseInt(s)));
    }

    private void validateRange(int integer){
        assert minValue <= integer && integer <= maxValue;
    }

    private void throwException() throws ParseException {
        throw new ParseException(this.getClass().getName() + "Invalid format", 0);
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public Map<String, Integer> getSpecialValues() {
        return specialValues;
    }

    public void setSpecialValues(Map<String, Integer> specialValues) {
        this.specialValues = specialValues;
    }

    public static final class Builder {
        private int minValue;
        private int maxValue;
        private Map<String, Integer> specialValues;

        private Builder() {
        }

        public Builder minValue(int minValue) {
            this.minValue = minValue;
            return this;
        }

        public Builder maxValue(int maxValue) {
            this.maxValue = maxValue;
            return this;
        }

        public Builder specialValues(Map<String, Integer> specialValues) {
            this.specialValues = specialValues;
            return this;
        }

        public Validator build() {
            return new Validator(this);
        }
    }

}
