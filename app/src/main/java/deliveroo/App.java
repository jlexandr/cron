/*
 * DISCLAIMER
 *
 * Yes, I know how to use Google,
 * I understand, that, probably, the most optimal way is just take ready lib,
 * but, as far it's a test task, let's have own solution
 */
package deliveroo;

import deliveroo.model.CronRecord;

import java.text.ParseException;

public class App {

    public String readCronString(String cronWithCommand) throws ParseException {
        CronRecord cronRecord = CronRecord.parseFromString(cronWithCommand.trim());
        return cronRecord.humanReadableFormat();
    }

    public static void main(String[] args) {
        try {
            String result = new App().readCronString(args[0]);
            System.out.println(result);
        } catch (ParseException parseException) {
            System.out.println("Can not read input: ".concat(parseException.getMessage()));
        }
    }

}
