# CRON decoder

Docker application

## Requirements:
JDK 11

## Project build

./gradlew clean jar

cd app/build/libs/

java -jar app.jar "*/15 0 1,15 * 1-5 /usr/bin/find"

## Notes
For test purpose you could use any string from test-input-data.csv

